FROM ruby:2.7-alpine
RUN apk add --no-cache --update git

ENV APP_PATH /var/apps/relay
WORKDIR $APP_PATH

COPY Gemfile* $APP_PATH/
RUN bundle install \
  --jobs `expr $(cat /proc/cpuinfo | grep -c "cpu cores") - 1` \
  --retry 3

COPY . $APP_PATH

CMD ["bin/run"]
